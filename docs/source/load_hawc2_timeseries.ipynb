{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Load a HAWC2 time-marching simulation\n",
    "\n",
    "HAWC2 is an aeroelastic multibody code, and we often use it to run time-marching simulations, such as the turbine's response to turbulence or a step-wind.\n",
    "\n",
    "HAWC2 saves the time-marching files in a tabular format in various data types. For the LAC course we use the GTSDF format, which is a space-efficient binary file with extension `.hdf5`. The lacbox includes an object called `ReadHAWC2` that can load this binary data into Python so you can analyse statistics, plot results, etc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from lacbox.io import ReadHAWC2\n",
    "from lacbox.test import test_data_path\n",
    "\n",
    "fname = Path(test_data_path) / 'dtu_10mw_turb.hdf5'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load the information in the binary file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "h2res = ReadHAWC2(fname)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Examine some of the attributes\n",
    "\n",
    "Some metadata is stored in a few different attributes. The most useful one is `.chaninfo`, which contains information on the different channels saved in the res file. The attribute is a list of 3 lists:  \n",
    "1. Names. A list of names for each channel, which is based on the type of output channel selected in your htc file. The names are chosen by HAWC2, unfortunately, not really descriptive or unique.  \n",
    "2. Units. The units of the channel. E.g., \"s\" is seconds.  \n",
    "3. Description. A longer description of the channel. This is how you can identify, e.g., to which body node the channel corresponds to.\n",
    "\n",
    "Let's examine this for the demo file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# print all (public) attributes just for info\n",
    "h2res.__dict__.keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "names, units, desc = h2res.chaninfo\n",
    "print('There are', len(names), 'channels in this output file.')\n",
    "print(names)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that there are 119 channels in this output file, but the names aren't super enlightening. Let's assume I want to plot the flapwise blade-root-moment of the 3 blades. Because I remember how HAWC2 coordinate systems work, it is probably the channels whose names are `Mx coo: blade1`, `Mx coo: blade2` and `Mx coo: blade3`, which correspond to a moment around the x-axis of the blade coordinate system. However, let's verify this and also identify the body and node by looking at the corresponding descriptions for those channels.\n",
    "\n",
    "Note that loading the file into pdap can be useful in investigating channels which channels are which because the program shows the name, units, and description in a format that is easier to parse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# find the indices that we think correspond to flapwise blade moment\n",
    "idx_blades = np.where(['Mx coo: blade' in name for name in names])[0]\n",
    "print('Identified indices of blade channels are', idx_blades)\n",
    "\n",
    "# print the corresponding description\n",
    "[print(desc[i]) for i in idx_blades]\n",
    "\n",
    "# print the coresponding units\n",
    "[print(units[i]) for i in idx_blades];"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `Mbdy:blade` confirms that the moment is taken on the blade body. The `blade 1 root` text at the end is optional text that I placed in the htc file to try and label the output channel as I wanted. The `nodenr:   3` indicates that, for the DTU 10 MW, the blade root moment is actually not taken at the root of the blade, but at the 3rd node, which is actually 6 m from the root of the blade. This is a choice made by the designers of the model.\n",
    "\n",
    "All three moments are given in kNm.\n",
    "\n",
    "## Analyse the data\n",
    "\n",
    "Let's quickly calculate some statistics and then plots the time series."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# assign the relevant data to variables for convenience\n",
    "Mx1, Mx2, Mx3 = h2res.data[:, idx_blades].T\n",
    "\n",
    "# calculate statistics\n",
    "print('Mean values:')\n",
    "[print(Mx.mean()) for Mx in [Mx1, Mx2, Mx3]]\n",
    "Mx_mean = h2res.data[:, idx_blades].mean(axis=1)\n",
    "\n",
    "# plot the time series\n",
    "fig, ax = plt.subplots(figsize=(12, 5))\n",
    "handles = ax.plot(h2res.t, h2res.data[:, idx_blades])\n",
    "l, = ax.plot(h2res.t, Mx_mean, 'k--')\n",
    "labels = [f'Blade {i+1}' for i in range(3)] + ['Sum of blades']\n",
    "ax.set(xlim=[100, 200],\n",
    "      xlabel='Time [s]',\n",
    "      ylabel='Blade root moment [kNm]')\n",
    "ax.legend(handles + [l], labels);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
