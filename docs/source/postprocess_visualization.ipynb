{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Post-processing and visualization\n",
    "\n",
    "If you have run many HAWC2 simulations, perhaps on a cluster or using a batch script, it is useful to visualize the statistics. The two steps are to first post-process and then visualize the results.\n",
    "\n",
    "## Post-processing\n",
    "\n",
    "Once the files have been generated, you can use the lacbox to calculate statistics from all of the simulations and save the results into an HDF5 file. The post-processing can be with or without the 10-minute damage-equivalent loads (DELs). Calculating without includes base statistics such as mean, standard deviation, max, min, etc. For more documentation on the post-processing options, please see the API tab to the left.\n",
    "\n",
    "Because post-processing is a heavy computational load, we will not demo it in detail in this notebook. However, here is some code to show you what it would look like.\n",
    "\n",
    "```\n",
    "res_dir = './res/'  # folder with HDF5 HAWC2 files\n",
    "save_path = 'stats.hdf5'  # where to save the stats file\n",
    "calc_del = True  # don't calculate damage-equivalent loads\n",
    "h2stats = process_statistics(res_dir, save_path, calc_del=calc_del)\n",
    "```\n",
    "\n",
    "## Visualization\n",
    "\n",
    "Below is some code demonstrating how you can use pandas to visualize the results in the HDF5 file.\n",
    "\n",
    "### Load the HDF5 file and examine its structure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "from lacbox.io import load_stats\n",
    "from lacbox.test import test_data_path\n",
    "\n",
    "stats_path = Path(test_data_path) / 'dtu_10mw_steady_stats.hdf5'\n",
    "h2stats, wsps = load_stats(stats_path, statstype='steady')\n",
    "type(h2stats)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `h2stats` object is essentially a `pandas.DataFrame` but with a few additions. Let's first familiarize ourselves with this object and the structure of the stats file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The stats file is located here:', h2stats.statspath)\n",
    "print('The shape is:', h2stats.shape)\n",
    "print('The column names are:', h2stats.columns)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are 9040 rows in this stats file, each corresponding to a single channel in a single file. The columns are:  \n",
    " * `path`: the full path to the HAWC2 time-marching result file  \n",
    " * `filename`: the name of the HAWC2 file  \n",
    " * `subfolder`: the name of the subfolder, if the htc file was in one  \n",
    " * `ichan`: the channel index, matching pdap (i.e., starting from 1)  \n",
    " * `names`: the name of the channel, matching pdap  \n",
    " * `units`: the units of the channel, matching pdap  \n",
    " * `desc`: the description of the channel, matching pdap  \n",
    " * `mean`, `max`, etc.: the value of the corresponding statistic  \n",
    " * `X%`: the value of the corresponding percentile  \n",
    " * `delX`: the 10-minute damage-equivalent fatigue load, calculated with the indicated value for the Wöhler exponent  \n",
    "\n",
    "This dataset is the DTU 10 MW operating with steady wind, no tower shadow, and no shear. The dataset includes four sets of simulations: with normal tilt, with no tilt, with no tilt and rigid tower/blades, and no tilt with rigid tower/blades and no aerodynamic drag. Each case was saved in a subfolder, and we can examine their names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "h2stats.subfolder.unique()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Isolate data of interest and make plots\n",
    "\n",
    "Let's make a plot of some data versus mean wind speed for the two cases to investigate the effects of tilt on mean loads.\n",
    "\n",
    "We want to be able to isolate channel based on a human-friendly identifier. The `filter_channel()` method in `HAWC2Stats` object can do that. But we need to define a dictionary that maps channel identifiers to substrings expected in that channel's description. Here is that dictionary for this model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "CHAN_DESCS = {'BldPit': 'pitch1 angle',  # blade pitch angle\n",
    "              'RotSpd': 'rotor speed',  # rotor speed\n",
    "              'Thrust': 'aero rotor thrust',  # thrust\n",
    "              'GenTrq': 'generator torque',  # generator torque\n",
    "              'ElPow': 'pelec',  # electrical poewr\n",
    "              'TbFA': 'momentmx mbdy:tower nodenr:   1',  # tower base FA\n",
    "              'TbSS': 'momentmy mbdy:tower nodenr:   1',  # tower base SS\n",
    "              'FlpBRM': 'momentmx mbdy:blade1 nodenr:   1 coo: blade1',  # flapwise blade-root moment\n",
    "              'EdgBRM': 'momentmy mbdy:blade1 nodenr:   1 coo: blade1',  # edgewise blade-root moment\n",
    "              }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's isolate two cases, perhaps with and without tilt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "tilt_stats = h2stats[h2stats.subfolder == 'tilt']\n",
    "notilt_stats = h2stats[h2stats.subfolder == 'notilt']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And finally let's plot our channels of interest in a quick plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_chans = ['RotSpd', 'BldPit', 'ElPow', 'GenTrq', 'TbFA', 'TbSS', 'FlpBRM', 'EdgBRM']\n",
    "fig, axs = plt.subplots(4, 2, num=1, figsize=(12, 10), clear=True)\n",
    "for i, chan_id in enumerate(plot_chans):\n",
    "    ax = axs.flatten()[i]\n",
    "    # get the mean value for that channel\n",
    "    tilt_chan = tilt_stats.filter_channel(chan_id, CHAN_DESCS)\n",
    "    notilt_chan = notilt_stats.filter_channel(chan_id, CHAN_DESCS)\n",
    "    # plot versus mean wsp\n",
    "    ax.plot(tilt_chan['wsp'], tilt_chan['mean'], 'o', label='With tilt')\n",
    "    ax.plot(notilt_chan['wsp'], notilt_chan['mean'], 'x', label='Without tilt')\n",
    "    # prettify\n",
    "    ax.grid()\n",
    "    ax.set(title=chan_id)\n",
    "\n",
    "axs[0, 0].legend()\n",
    "\n",
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that tilt does not significantly impact the steady-state operational values for the turbine or the tower-base fore-aft moment. However, it does have an impact on the tower-base side-side moment and some impact on the blades as well.\n",
    "\n",
    "Let's identify which simulation has the largest tower-base for-aft moment for both cases."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chan_id = 'TbFA'\n",
    "\n",
    "idx_max = tilt_stats.filter_channel(chan_id, CHAN_DESCS)['mean'].idxmax()\n",
    "print('Max-TBFA simulation with tilt:', tilt_stats.loc[idx_max, 'filename'])\n",
    "print('  Max load:', tilt_stats.loc[idx_max, 'mean'], 'kNm')\n",
    "\n",
    "idx_max = notilt_stats.filter_channel(chan_id, CHAN_DESCS)['mean'].idxmax()\n",
    "print('Max-TBFA simulation without tilt:', notilt_stats.loc[idx_max, 'filename'])\n",
    "print('  Max load:', notilt_stats.loc[idx_max, 'mean'], 'kNm')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As expected, both have peaks at 11 m/s, which is near rated."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "mylac",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.14"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
