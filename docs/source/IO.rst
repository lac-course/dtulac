File IO
=======

HAWC inputs
------------
.. toctree::
   :maxdepth: 2

   load_save_ae
   load_save_c2def
   load_save_pc
   load_save_st

HAWC output
------------
.. toctree::
   :maxdepth: 2

   load_pwr_ind
   load_cmb_amp
   load_ctrl_txt
   load_hawc2_timeseries
   load_hawc2_statistics

   


