API 
===

lacbox.io
---------

.. automodule:: lacbox.io
    :members:

lacbox.aero
-----------

.. automodule:: lacbox.aero
    :members: root_chord, min_tc_chord, max_twist, interpolator

lacbox.htc.HTCFile
------------------

.. autoclass:: lacbox.htc.HTCFile
   :members: add_mann_turbulence, save, set_name, set_time

lacbox.postprocess
------------------

.. automodule:: lacbox.postprocess
    :members: process_statistics