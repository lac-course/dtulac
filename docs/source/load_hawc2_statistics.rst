Load HAWC2 statistics
======================

You can load a statistics file post-processed using `lacbox.postprocess.process_statistics()`.
An example of how you call this function and then how you can load/manipulate the resulting file
can be found in the "Running many simulations" section.
