# The lacbox

This package contains functions that are useful in DTU Wind Energy course
46320: Loads, Aerodynamics, and Control of Wind Turbines.

## Installation and updating
It is recomented to install`lacbox` via pip:
```sh
pip install lacbox
```
If you have lacbox installed and need the newest version, you can upgrade:
```sh
pip install --upgrade lacbox
```

If you want the files locally to look at/modify the source code, first clone
(requires git) or download the package from the GitLab repository. Open an
Anaconda terminal in the `dtulac` folder and enter `pip install -e .`. Don't
forget the period at the end! This creates an "editable" installation, so any 
changes you make to the files will impact the package.

## Documentation
The project documentation can be found here:
[https://lac-course.pages.windenergy.dtu.dk/dtulac](https://lac-course.pages.windenergy.dtu.dk/dtulac)

## Source code

The source code for the project is here:
[https://gitlab.windenergy.dtu.dk/lac-course/dtulac](https://gitlab.windenergy.dtu.dk/lac-course/dtulac)

## Authors and acknowledgment
Contributor list:  
 - Jenni Rinker
 - Kenneth Lønbæk
 - Rasmus Sode Lund

## License
This project is licensed under MIT license.
